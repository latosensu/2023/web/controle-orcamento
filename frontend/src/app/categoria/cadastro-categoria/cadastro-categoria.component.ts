import { CategoriaApiService } from './../../core/categoria-api.service';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Categoria } from 'src/app/core/model/categoria';

@Component({
  selector: 'app-cadastro-categoria',
  templateUrl: './cadastro-categoria.component.html',
  styleUrls: ['./cadastro-categoria.component.scss']
})
export class CadastroCategoriaComponent {
  cadastroCategoriaForm = this.fb.group({
    nome: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    descricao: [null, Validators.required],
    postalCode: [null, Validators.compose([
      Validators.required, Validators.minLength(5), Validators.maxLength(5)])
    ],
    tipo: ['ENTRADA', Validators.required]
  });

  constructor(private fb: FormBuilder, private router: Router, private categoriaApiService: CategoriaApiService) {}

  onSubmit(): void {

    const nome = this.cadastroCategoriaForm.get('nome')?.value;
    const tipo = this.cadastroCategoriaForm.get('tipo')?.value;
    const descricao = this.cadastroCategoriaForm.get('descricao')?.value;
    this.categoriaApiService.addCategoria(nome, tipo, descricao).then((categoria: Categoria) => {
      alert(`Obrigado. A categoria ${categoria.nome} foi adicionada com sucesso!`);
      this.router.navigateByUrl('/categoria');
    }).catch((error) => {
      alert(`Não foi possível adicionar a categoria por causa do erro:  ${error.message}!`);
    });

  }

  voltarParaCategorias = () => {
    this.router.navigateByUrl('/categoria')
  }
}
