import { Categoria } from "./categoria";

export interface Lancamento {
  id: number;
  categoria: Categoria;
  descricao: string;
  valor: number;
  dataBalancete: string;
}
