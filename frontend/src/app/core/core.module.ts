import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CategoriaApiService } from './categoria-api.service';
import { ConverteMesPipe } from './utils/converte-mes.pipe';



@NgModule({
  declarations: [
    ConverteMesPipe
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    CategoriaApiService,
  ],
  exports: [
    HttpClientModule,
    ConverteMesPipe
  ]
})
export class CoreModule { }
