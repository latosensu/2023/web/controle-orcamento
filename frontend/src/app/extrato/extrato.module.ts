import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';
import { UiModule } from '../ui/ui.module';
import { ExtratoRoutingModule } from './extrato-routing.module';
import { ExtratoComponent } from './extrato.component';

@NgModule({
  declarations: [
    ExtratoComponent
  ],
  imports: [
    CommonModule,
    ExtratoRoutingModule,
    UiModule,
    CoreModule
  ]
})
export class ExtratoModule { }
