import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExtratoComponent } from './extrato.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'prefix',
    redirectTo: 'exibir-extrato'
  },
  {
    path: 'exibir-extrato',
    pathMatch: 'full',
    component: ExtratoComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
  ]
})
export class ExtratoRoutingModule { }
