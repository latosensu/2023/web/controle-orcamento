import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Extrato } from '../core/model/extrato';

@Component({
  selector: 'app-extrato',
  templateUrl: './extrato.component.html',
  styleUrls: ['./extrato.component.scss']
})
export class ExtratoComponent implements OnInit {

  columnsToDisplay = ['dataBalancete', 'categoria', 'descricao', 'valor' ];

  anos: number[] = [];
  anoSelecionado: number = 1970;
  meses: number[] = [];
  mesSelecionado: number = 1;
  anoAtual: number = 1970;
  mesAtual: number = 1;
  extrato: Extrato = {} as Extrato;
  dataAtual = new Date();

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.anoAtual = this.dataAtual.getFullYear() ;
    this.anoSelecionado = this.anoAtual;
    this.mesAtual = this.dataAtual.getMonth() + 1;
    this.mesSelecionado = this.mesAtual;
    this.atualizarAnos();
    this.atualizarMeses();

    this.obterExtrato();
  }

  private atualizarAnos  = () => {
    for (let i = this.anoAtual; i >= this.anoAtual - 4; i--) {
      this.anos.push(i);
    }
  }

  private atualizarMeses = () => {
    this.meses = [];
    let ultimoMes = 12;
    if (this.anoSelecionado=== this.anoAtual) {
      ultimoMes = this.mesAtual;
    }
    for (let i = 1; i <= ultimoMes; i++) {
        this.meses.push(i);
    }
  }

  alterarAno = (event: any) => {
    this.anoSelecionado = event.value;
    console.log(event);
    this.atualizarMeses();

  }

  alterarMes = (event: any) => {
    this.mesSelecionado = event.value;
    console.log(event);


  }

  obterExtrato = () => {
    console.log("Ano selecionado", this.anoSelecionado);
    console.log("Mês selecionado", this.mesSelecionado);
    const mesExtrato = (this.mesSelecionado<10)?"0"+this.mesSelecionado.toString():this.mesSelecionado.toString();
    var ultimoDiaDoMes =
      this.mesSelecionado===this.mesAtual && this.anoSelecionado === this.anoAtual?
        this.dataAtual.getUTCDate().toString():
        new Date(this.anoSelecionado, this.mesSelecionado, 0).getUTCDate().toString();

    this.httpClient.get<Extrato>(
      environment.apiUrl + '/extrato', {
        params: {
          dataInicio: this.anoSelecionado + '-' + mesExtrato + '-' + '01',
          dataFim: this.anoSelecionado + '-' + mesExtrato + '-' + ultimoDiaDoMes,
        }
      }).toPromise().then((extrato: Extrato) => {
      this.extrato = extrato;
      console.log(extrato)
    }).catch((error) => {
      console.log(error);
    });
  }

}
