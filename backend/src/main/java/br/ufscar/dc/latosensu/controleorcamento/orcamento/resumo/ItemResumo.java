package br.ufscar.dc.latosensu.controleorcamento.orcamento.resumo;

import java.math.BigDecimal;

import br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria.TipoCategoria;
import lombok.Data;

@Data
public class ItemResumo {

    public ItemResumo(String nome, TipoCategoria tipo, BigDecimal valorTotalCategoria) {
        this.nomeCategoria = nome;
        this.tipoCategoria = tipo.name();
        this.valorTotalCategoria = valorTotalCategoria;
    }
    String nomeCategoria;
    BigDecimal valorTotalCategoria; 
    String tipoCategoria;
    
}
