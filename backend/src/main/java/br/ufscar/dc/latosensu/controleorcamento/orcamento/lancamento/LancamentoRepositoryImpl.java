package br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Implementa os métodos do repository de l-assaszançamento
 */
@Repository
public class LancamentoRepositoryImpl
  extends SimpleJpaRepository<Lancamento, Long>
  implements LancamentoRepository<Lancamento, Long> {

  public LancamentoRepositoryImpl(EntityManager em) {
    super(Lancamento.class, em);
  }

  @PersistenceContext
  private EntityManager entityManager;

  @Transactional
  public List<Lancamento> findAllByDescricaoILikeUltra(String descricao) {
    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
    CriteriaQuery<Lancamento> criteriaQuery = criteriaBuilder.createQuery(
      getDomainClass()
    );
    Root<Lancamento> root = criteriaQuery.from(getDomainClass());

    criteriaQuery
      .select(root)
      .where(
        criteriaBuilder.like(
          root.<String>get("descricao"),
          "%" + descricao + "%"
        )
      );

    TypedQuery<Lancamento> query = entityManager.createQuery(criteriaQuery);

    return query.getResultList();
  }

  @Override
  public List<Lancamento> findByDataBalanceteBetween(LocalDate date, LocalDate date2) {
    CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
    CriteriaQuery<Lancamento> criteriaQuery = criteriaBuilder.createQuery(
      getDomainClass()
    );
    Root<Lancamento> root = criteriaQuery.from(getDomainClass());

    criteriaQuery
      .select(root)
      .where(
        criteriaBuilder.between(
          root.<LocalDate>get("dataBalancete"),
          date,
          date2
        )
      );

    TypedQuery<Lancamento> query = entityManager.createQuery(criteriaQuery);

    return query.getResultList();
  }
}
