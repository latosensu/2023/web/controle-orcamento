package br.ufscar.dc.latosensu.controleorcamento.orcamento.extrato;

import br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria.TipoCategoria;
import br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento.Lancamento;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import lombok.Data;

@Data
public class Extrato {

  public Extrato(
    List<Lancamento> todosLancamentos,
    LocalDate dataInicial,
    LocalDate dataFinal
  ) {
    this.dataInicial = dataInicial;
    this.dataFinal = dataFinal;
    this.lancamentos = this.filtrarLancamentos(todosLancamentos);
    this.saldo = this.calcularSaldo(todosLancamentos);
    this.resultadoPeriodo = this.calcularSaldo(this.lancamentos);
  }

  private List<Lancamento> filtrarLancamentos(
    List<Lancamento> todosLancamentos
  ) {
    Predicate<Lancamento> dataNoPeriodoConsideradoPeloExtPredicate = (Lancamento lancamento) -> {
      LocalDate dataBalancete = lancamento.getDataBalancete();
      return (
        dataBalancete.compareTo(this.dataInicial) >= 0 &&
        dataBalancete.compareTo(this.dataFinal) <= 0
      );
    };
    return todosLancamentos
      .stream()
      .filter(dataNoPeriodoConsideradoPeloExtPredicate)
      .collect(Collectors.toList());
  }

  private Double calcularSaldo(List<Lancamento> todosLancamentos) {
    return todosLancamentos
      .stream()
      .map(
        (Lancamento lancamento) -> {
          BigDecimal multiplicador = BigDecimal.valueOf(
            lancamento.getCategoria().getTipo().equals(TipoCategoria.ENTRADA)
              ? 1
              : -1
          );

          return lancamento.getValor().multiply(multiplicador).doubleValue();
        }
      )
      .reduce(
        (Double a, Double b) -> {
          return a += b;
        }
      )
      .orElse(Double.valueOf(0));
  }

  private List<Lancamento> lancamentos;
  private Double saldo;
  private Double resultadoPeriodo;
  private LocalDate dataInicial;
  private LocalDate dataFinal;
}
