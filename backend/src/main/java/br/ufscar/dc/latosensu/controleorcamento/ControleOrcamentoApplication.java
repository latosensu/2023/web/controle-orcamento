package br.ufscar.dc.latosensu.controleorcamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Classe que inicializa a aplicação
 */
@SpringBootApplication
@EnableJpaRepositories
public class ControleOrcamentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControleOrcamentoApplication.class, args);
	}

}
