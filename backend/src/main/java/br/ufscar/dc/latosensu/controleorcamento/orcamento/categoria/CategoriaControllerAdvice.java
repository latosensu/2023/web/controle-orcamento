package br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria;

import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Tratamento das exceções lançadas pelo {@link CategoriaController}
 */
@ControllerAdvice
public class CategoriaControllerAdvice {

  @ResponseBody
  @ExceptionHandler(CategoriaNaoEncontradaException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  HashMap<String, String> CategoriaNaoEncontradaExceptionHandler(
    CategoriaNaoEncontradaException excecao
  ) {
    HashMap<String, String> retornoExcecao = new HashMap<>();
    retornoExcecao.put("message", excecao.getMessage());
    return retornoExcecao;
  }
}
